HTML
----
    
1. ¿Cuál es la utilidad del atributo "alt" en las imágenes y por qué es
   importante?

   El atributo "alt" se utiliza para describir el contenido de la imagen. Es importante porque en caso de no cargar la imagen o si se utiliza un lector de pantalla, se muestra la descripcion de la imagen.

2. ¿Cuáles son los elementos y los atributos fundamentales en un formulario web?

   Los elementos y atributos mas fundamentales de un formiario web son: 
   1- <form> con sus atributos "action" y "method"
   2- <input> con sus atributos "type", "id" y "name"
   3- <label> con sus atributo "for"

CSS
---
    
1. ¿Cuál es el selector que se aplica sobre todos los elementos p dentro de un
   elemento div?

    `div p`

    `div.p` 

    `div + p`

    `div > p`

    `p`

    `div, p`

   El selector que aplica a todos los elementos "p" que esten dentro de un elemento div es "div p"

2. Explique qué significan la siguientes reglas de css

    `background: #000 url(images/bg.gif) no-repeat top right;

    Se le aplica una imgen de fondo que no se repite, en caso de no cargar la imagen se aplica un color de fondo negro, y se ubica arriba a la derecha.

    margin: 1em 2em 0;

    Se le aplica un margen superior de 1em, un margen izquierdo y derecho de 2 em y un margen inferior de 0em.

    padding: 1em 2em;
    
    Se le aplica un espacio de relleno superior e inferior de 1em, un espacio de relleno izquierdo y derecho de 2 em.

    border-size: 2px;

    Se aplica un tamaño a los bordes del elemento de 2px.
    
    

JavaScript
----------

1. ¿Cuál es la sintaxis correcta para cambiar todos los párrafos en un documento
   HTML?

    `document.getElementByName("p").innerHTML = "Hola mundo!"`
    
    `document("p").innerHTML = "Hola mundo!"`
    
    `p.setInnerHTML("Hola mundo!")`
    
    `document.querySelectorAll("p").forEach(function(para) {para.innerHTML = 'Hola' })`   
    
    
   La sintaxis correcta para camibiar todos los parrafos de un documento HTML es: "document.querySelectorAll("p").forEach(function(para){para.innerHTML = 'Hola'})"

2. ¿Cuál es la funcion de javascript en un documento HTML?

   Javascript cumple la funcion de añadir interacividad dinámica en los documentos HTML con los usuarios.

3. ¿Javascript y Java son el mismo lenguaje?

   Javascript y Java son dos lenguajes de programacion distintos.

4. ¿Por qué es tan popular JavaScript, es el mejor lenguaje de programación?

   JavaScript es popular porque es uno de los lenguajes más usados a nivel mundial y su curva de aprendizaje es una de las más sencillas a comparación de otros lenguajes, por lo cual también es muy recomendado para aprender a programar.